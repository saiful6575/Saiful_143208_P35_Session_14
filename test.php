<!DOCTYPE html>
<head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style>
        .form-horizontal{
            background-color: lightgrey;
            width: 650px;
            border: 5px solid cadetblue;
            padding: 25px;
            margin: 15px;
        }

    </style>
<style>
    h3 {
        text-align: center;
        text-decoration-color: aqua;
    }
</style>
</head>
 <body>

 <form class="form-horizontal">
     <br>
     <h3>Add Book List</h3>
     <div class="form-group">
         <label for="inputEmail3" class="col-sm-2 control-label">Enter ID:</label>
         <div class="col-sm-6">
             <input type="email" class="form-control" id="inputEmail3" placeholder="Book ID">
         </div>
     </div>
     <div class="form-group">
         <label for="inputPassword3" class="col-sm-2 control-label">Enter Book Name</label>
         <div class="col-sm-6">
             <input type="password" class="form-control" id="inputPassword3" placeholder="Book Name">
         </div>
     </div>

     <div class="form-group">
         <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-default">Submit</button>
         </div>
     </div>
 </form>

 <!-- Latest compiled and minified JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 </body>

</html>