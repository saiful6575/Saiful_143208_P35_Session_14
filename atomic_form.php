<!DOCTYPE html>
<html>
<head>
    <style>
        ul.pagination {
            display: inline-block;
            padding: 0;
            margin: 0;
        }

        ul.pagination li {display: inline;}

        ul.pagination li a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
        }

        ul.pagination li a.active {
            background-color: #4CAF50;
            color: white;
        }

        ul.pagination li a:hover:not(.active) {background-color: #ddd;}
    </style>
    <style>
        .button {
            background-color: #5673af; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 6px;
            cursor: pointer;
        }

        .button1 {font-size: 10px;}

    </style>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h2>Colored Table Header</h2>

<table>
    <tr>
        <th>Sl</th>
        <th>ID</th>
        <th>Bookname</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>01</td>
        <td>01</td>
        <td>Physics-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>02</td>
        <td>02</td>
        <td>Physics-II</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td>03</td>
        <td>03</td>
        <td>Chemistry-I</td>
        <td>
            <button class="button button1">View</button>
            <button class="button button1">edit</button>
            <button class="button button1">delete</button>
            <button class="button button1">trash</button></td>
    </tr>
    <tr>
        <td colspan="4"><ul class="pagination">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a class="active" href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">»</a></li>
            </ul></td>

    </tr>





</table>

<button class="button button1">Create new</button>
<button class="button button1">Download as PDF</button>
<button class="button button1">View trash</button><br>

<button class="button button1">Download as CSV</button><br>
<button class="button button1">Download as PNG</button>
<button class="button button1">Send email</button>



</body>
</html>

