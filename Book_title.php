
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<table style="text-align: center">
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>5</td>
        <td>PHP - BOOK TITLE #1</td>
        <td><button type="button">View</button><button type="button">Edit</button><button type="button">Delete</button><button type="button">Trash</button></td>

    </tr>
    <tr>
        <td>2</td>
        <td>6</td>
        <td>PHP - BOOK TITLE #2</td>
        <td><button type="button">View</button><button type="button">Edit</button><button type="button">Delete</button><button type="button">Trash</button></td>

    </tr>
    <tr>
        <td>3</td>
        <td>7</td>
        <td>PHP - BOOK TITLE #3</td>
        <td><button type="button">View</button><button type="button">Edit</button><button type="button">Delete</button><button type="button">Trash</button></td>

    </tr>

    <tr style="text-align: left">
        <td colspan="4">Previous: <1,2,3,4,5,6,7> :Next</td>

    </tr>

    <tr>
        <td colspan="4"><button type="button">Add New Book Title</button>   <button type="button">View Trash Items</button><button type="button">Download as PDF</button><button type="button">Download as EXCEL File</button></td>
    </tr>
</table>


</body>
</html>

